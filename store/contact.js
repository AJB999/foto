


import axios from 'axios'

export const state = () => ({
	contactInfo : {}
})

export const mutations = {
	setContactInfo(state, data) {
		state.contactInfo = data
	}
}

export const actions = {
	async getContactInfo({ commit }) {
		const result = await axios.get('http://foto.aaronjban.com.s3.amazonaws.com/static/contact.json')
		commit('setContactInfo', result.data)
	}
}
