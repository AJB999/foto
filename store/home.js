


import axios from 'axios'

export const state = () => ({
	hero : {}
})

export const mutations = {
	setHero(state, data) {
		state.hero = data
	}
}

export const actions = {
	async getHero({ commit }) {
		const result = await axios.get('http://foto.aaronjban.com.s3.amazonaws.com/static/home.json')
		commit('setHero', result.data)
	}
}
