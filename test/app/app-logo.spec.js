


import { shallowMount } from '@vue/test-utils'
import appLogo from '@/components/app/app-logo.vue'



describe('appLogo', () => {

	const wrapper = shallowMount(appLogo)

	test('is a Vue instance', () => {
		expect(wrapper.isVueInstance()).toBeTruthy()
	})

	test('renders correctly', () => {
		expect(wrapper.element).toMatchSnapshot()
	})

	test('has the correct logo text', () => {
		const $html = wrapper.html()
		expect($html).toContain('FOTO')
	})

})
